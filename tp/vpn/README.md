# TP VPN

# Présentation générale

# O. Prerequisites

Vous pouvez encore cloner la VM installée précédemment pour ce TP.

Comme aux TPs précédents, désactivez SELinux.

Définissez un nom d'hôte et un nom de domaine à votre serveur. Dans le reste du TP, on considérera le nom `vpn.cesi`.

# I. CA

## Présentation

On met ici en place une autorité de certification (CA) qui sera utilisée pour signer le certificats du serveur et des clients.

Pour rappel, une autorité de certification est un acteur qui possède une paire de clés (sa clé publique est stockée dans son certificat) et qui va "signer" (= chiffrer avec sa clé privée) le certificat de tous les autres acteurs (clients et serveurs) afin d'établir une relation de confiance entre eux.

Il est récurrent d'avoir une machine dédiée, qui portera les clés et qui signera les certificats des autres machines. A des fins de simplicité, nous utiliserons une unique machine qui sera à la fois CA, et serveur VPN.

Le rôle de la CA sera donc de signer le certificat du serveur VPN et de chacun des clients qui voudront s'y connecter dans notre cas. On fera ces opérations à la main dans notre cas.

# Setup

## EasyRSA

Récupération de [EasyRSA](https://github.com/OpenVPN/easy-rsa/releases), un outil fourni par OpenVPN pour gérer les clés et certificats plus facilement :
```bash
# On se déplace dans le répertoire personnel de l'utilisateur courant
$ cd

# Récupération de EasyRSA. Dernière version à jour au 06/01/2021.
$ curl -SLO https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.8/EasyRSA-3.0.8.tgz

# Création d'un répertoire dédié à la CA
$ mkdir ~/ca

# Extraction de l'archive récupérée avec le curl
$ tar xvzf EasyRSA-3.0.8.tgz

# On déplace tous les fichiers dans le répertoire ca/
$ mv EasyRSA-3.0.8/* ca/
```

## Initialisation de la CA

```bash
$ cd ~/ca
$ cp vars.example vars
```

Il faut désormais éditer le fichier `vars` que l'on vient de créeret modifier les lignes suivantes (vous pouvez mettre ce que vous voulez comme infos dans le cadre du TP) :

```
set_var EASYRSA_REQ_COUNTRY    "FR"
set_var EASYRSA_REQ_PROVINCE   "Gironde"
set_var EASYRSA_REQ_CITY       "Bordeaux"
set_var EASYRSA_REQ_ORG        "CESI"
set_var EASYRSA_REQ_EMAIL      "cesi@cesi.com"
set_var EASYRSA_REQ_OU         "Bap"
```

On initialise une nouvelle PKI :
```bash
$ cd ~/ca
$ ./easyrsa init-pki
```

On va générer la clé privée et le certificat (pour rappel, il contient la clé publique) de la CA :

```bash
$ cd ~/ca
$ ./easyrsa build-ca nopass
```

# II. OpenVPN Server

## Installation d'OpenVPN

```bash
$ sudo yum install -y epel-release
$ sudo yum install -y openvpn
```

## EasyRSA

Récupération de [EasyRSA](https://github.com/OpenVPN/easy-rsa/releases) pour le serveur aussi. Pour rappel, la CA et le serveur VPN peuvent être deux machines différentes.

```bash
# On se déplace dans le répertoire personnel de l'utilisateur courant
$ cd

# Récupération de EasyRSA. Dernière version à jour au 06/01/2021.
$ curl -SLO https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.8/EasyRSA-3.0.8.tgz
                              
# Création d'un répertoire dédié au serveur VPN
$ mkdir ~/ovpn

# Extraction de l'archive récupérée avec le curl
$ tar xvzf EasyRSA-3.0.8.tgz

# On déplace tous les fichiers dans le répertoire ovpn/
$ mv EasyRSA-3.0.8/* ovpn/
```

Initialisation d'une PKI là aussi :
```bash
$ cd ~/ovpn
$ ./easyrsa init-pki
```

## DH

DH (pour Diffie-Hellman) est une méthode qui permet d'échanger de façon sécurisée des clés. Il est nécessaire de générer une clé pour qu'il soit opérationnel :
```bash
$ cd ~/ovpn
$ ./easyrsa gen-dh 
$ sudo cp ./pki/dh.pem /etc/openvpn/
```

## Clé et certificat du serveur

On va générer une clé et une demande de signature de certificat (ou CSR pour Certificate Signing Request). On soumettra ensuite cette demande de signature à notre CA, afin qu'elle signe le certificat du serveur.

```bash
$ cd ~/ovpn
$ ./easy-rsa gen-req cesi_vpn nopass
$ sudo cp ~/ovpn/pki/private/cesi_vpn.key /etc/openvpn/
```

On signe le demande de signature de certificat du serveur :

```bash
$ cd ~/ca
$ ./easyrsa import-req ~/ovpn/pki/reqs/cesi_vpn.req cesi_vpn
$ ./easyrsa sign-req server cesi_vpn
```

Récupération dans la conf du serveur VPN du certificat signé, ainsi que le certificat de la CA :

```bash
$ cp ~/ca/pki/issued/cesi_vpn.crt /etc/openvpn
$ cp ~/ca/pki/ca.crt /etc/openvpn
```

La partie crypto est en place, on configure maintenant le serveur OpenVPN :
```bash
# Un modèle de conf est fourni avec le paquet openvpn, on le récupère :
$ sudo cp /usr/share/doc/openvpn-2.4.10/sample/sample-config-files/server.conf /etc/openvpn/cesi_vpn.conf

# Edition du fichier de configuration de OpenVPN
$ sudo vim /etc/openvpn/cesi_vpn.conf

# Les commentaires du fichier de config sont explicites, lisez les :)
# Votre conf (si on exclut les commentaires) doit ressembler à :
$ sudo cat /etc/openvpn/cesi_vpn.conf
port 1194
proto udp
dev tun
ca ca.crt
cert cesi_vpn.crt
key cesi_vpn.key  # This file should be kept secret
dh dh.pem
server 10.8.0.0 255.255.255.0
ifconfig-pool-persist ipp.txt
push "redirect-gateway def1 bypass-dhcp"
push "dhcp-option DNS 208.67.222.222"
push "dhcp-option DNS 208.67.220.220"
keepalive 10 120
tls-auth ta.key 0 # This file is secret
cipher AES-256-CBC
user nobody
group nobody
persist-key
persist-tun
status openvpn-status.log
verb 3
explicit-exit-notify 1
auth SHA256
```

Démarrage du service OpenVPN :

```bash
# Démarrage du service
$ sudo systemctl start openvpn@cesi_vpn

# Activation du service au démarrage de la machine
$ sudo systemctl enable openvpn@cesi_vpn
```

Pour vérifier que votre serveur est opérationnel :
```bash
# Nouvelle carte réseau dédiée au VPN
$ ip a

# Service OpenVPN démarré
$ sudo systemctl status openvpn@cesi_vpn

# Port à l'écoute en UDP
$ sudo ss -alupn
```

## Configuration réseau

On autorise le forwarding IPv4 :
```bash
$ echo 'net.ipv4.ip_forward = 1' | sudo tee -a /etc/sysctl.conf
```

FirewallD (le firewall de CentOS) gère une notion de "zones". Ces zones permettent de mettre en places des jeux de règles de firewalling différentes, en fonction d'interfaces spécifiques.

On va utiliser la zone "trusted" qui existe par défaut (c'est la zone "public" qu'on utilisait jusqu'alors, lorsque rien n'est précisé explicitement).

```bash
# On ajoute l'interface du VPN à la zone trusted
$ sudo firewall-cmd --permanent --zone=trusted --add-interface=tun0

# Autorisation du trafic de OpenVPN dans le firewall
$ sudo firewall-cmd --permanent --add-service openvpn

# Autorisation du masquerading
$ sudo firewall-cmd --permanent --zone=trusted --add-masquerade 

# Repérez le nom de l'interface NAT qui permet de joindre l'extérieur
$ ip r s # interface utilisée par la route par défaut

# Règle avancée pour autoriser les clients connectés au VPN d'être routés vers internet
$ sudo firewall-cmd --permanent --direct --passthrough ipv4 -t nat -A POSTROUTING -s  10.8.0.0/24 -o <INTERFACE_NAME> -j MASQUERADE
# Par exemple, si c'est enp0s3 qui est la carte NATée :
$ sudo firewall-cmd --permanent --direct --passthrough ipv4 -t nat -A POSTROUTING -s  10.8.0.0/24 -o enp0s3 -j MASQUERADE

# Reload du firewall
$ sudo firewall-cmd --reload
```

# III. Client

On crée des dossiers dédiés aux fichiers des clients : 
```bash
$ mkdir -p ~/openvpn-clients/files # clé et cert des clients
$ mkdir ~/openvpn-clients/configs # fichers de conf utilisables par les clients
$ mkdir ~/openvpn-clients/base # fichiers communs à tous les clients
```

Récupération des infos du serveur pour les clients :
```bash
$ cp ~/ovpn/ta.key ~/openvpn-clients/base/
$ cp /etc/openvpn/ca.crt ~/openvpn-clients/base/
```

Il existe un fichier de configuration d'exemple pour les clients aussi, on le récupère :
```bash
$ sudo cp /usr/share/doc/openvpn-2.4.10/sample/sample-config-files/client.conf ~/openvpn-clients/base/
```

Editez le fichier de configuration (avec `vim` par exemple) pour qu'il ressemble à :
```bash
client
dev tun
proto udp
remote <IP_HOST_ONLY_DU_SERVEUR_VPN> 1194
resolv-retry infinite
nobind
persist-key
persist-tun
remote-cert-tls server
cipher AES-256-CBC
verb 3
auth SHA256
key-direction 1
```

Créer un nouveau fichier, un script, qui permettra de générer simplement une clé et un certificat pour nos clients :
```bash
$ cat ~/openvpn-clients/gen_config.sh
#!/bin/bash

FILES_DIR=$HOME/openvpn-clients/files
BASE_DIR=$HOME/openvpn-clients/base
CONFIGS_DIR=$HOME/openvpn-clients/configs

BASE_CONF=${BASE_DIR}/client.conf
CA_FILE=${BASE_DIR}/ca.crt
TA_FILE=${BASE_DIR}/ta.key

CLIENT_CERT=${FILES_DIR}/${1}.crt
CLIENT_KEY=${FILES_DIR}/${1}.key

# Test for files
for i in "$BASE_CONF" "$CA_FILE" "$TA_FILE" "$CLIENT_CERT" "$CLIENT_KEY"; do
    if [[ ! -f $i ]]; then
        echo " The file $i does not exist"
        exit 1
    fi

    if [[ ! -r $i ]]; then
        echo " The file $i is not readable."
        exit 1
    fi
done

# Generate client config
cat > ${CONFIGS_DIR}/${1}.ovpn <<EOF
$(cat ${BASE_CONF})
<key>
$(cat ${CLIENT_KEY})
</key>
<cert>
$(cat ${CLIENT_CERT})
</cert>
<ca>
$(cat ${CA_FILE})
</ca>
<tls-auth>
$(cat ${TA_FILE})
</tls-auth>
EOF
```

On rend le fichier exécutable :
```bash
$ chmod u+x ~/openvpn-clients/gen_config.sh
```

Génération d'une clé et d'une demande de signature de certificat pour notre client :
```bash
$ cd ~/ovpn
$ ./easyrsa gen-req cesi_client nopass
$ cp pki/private/client1.key ~/openvpn-clients/files/
```

Signature de la demande de signature de certificat avec notre CA :
```bash
$ cd ~/ca
$ ./easyrsa import-req /tmp/cesi_client.req cesi_client
$ ./easyrsa sign-req client cesi_client
```

Récupération du certificat du client signé :
```bash
$ cp ~/ca/pki/issued/cesi_client.crt ~/openvpn-clients/files
$ cd ~/openvpn-clients
$ ./gen_config.sh cesi_client
```

Cette dernière commande a généré un fichier `~/openvpn-clients/files/cesi_client.ovpn`. Récupérez ce fichier sur votre poste.

Installez un client OpenVPN sur votre PC et utilisez le fichier `cesi_client.ovpn` pour vous connecter à votre serveur.
